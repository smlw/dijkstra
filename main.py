import numpy as np


def main():
    # Расстояния от вершины до вершины
    lenghts = np.matrix(
        '100 1 2 3 100 100 100 100 100 100;'
        '1 100 100 100 2 100 100 100 100 100;'
        '2 100 100 100 1 100 100 100 100 100;'
        '3 100 100 100 100 100 100 100 6 100;'
        '100 2 1 100 100 1 2 100 100 100;'
        '100 100 100 100 1 100 3 100 100 100;'
        '100 100 100 100 2 3 100 2 100 100;'
        '100 100 100 100 100 100 2 100 100 2;'
        '100 100 100 100 100 100 4 100 100 5;'
        '100 100 100 100 100 100 100 2 5 100'
    )

    # Веса вершин
    vertexs = np.matrix(
        '0 10000 10000 10000 10000 10000 10000 10000 10000 10000'
    )

    # Множество посещенных вершин
    flags = np.matrix(
        '0 0 0 0 0 0 0 0 0 0'
    )

    for i in range(10):
        currentVertex = lenghts[i]  # Текущая вершина
        currentFlag = flags.item(i)  # Посещена ли вершина
        if currentFlag == 0:  # Проверяем пройденная или веришна
            for s in range(10):
                currentVertext = currentVertex.item(s)  # Каждое расстояние до вершины
                if currentVertext != 100:  # Берем только существующие вершины
                    result = vertexs.item(i) + currentVertext  # Складываем вес вершины и вес пути
                    if result <= vertexs.item(s):  # Проверяем c текущим весом вершины
                        vertexs.itemset(s, result)  # Переписываем, если вес меньше
            flags.itemset(i, 1)  # В общей матрице флагов помечаем вершину как пройденную

    # i - от какой вершины
    # currentVertext - Расстояние
    # s - до какой вершины

    print(flags)
    print(vertexs)

    optimalWay = []  # Запишем путь
    endPoint = 9  # Последняя вершина
    startPoint = 0  # Первая вершина

    optimalWay.append(endPoint)

    # Ищем маршрут
    while endPoint > startPoint:
        for s in range(10):
            # Бежим по расстояниям предыдущей вершины
            currentVertex2 = lenghts[endPoint].item(s)  # Расстояние предыдущей вершины
            if currentVertex2 != 100:
                newVertex = vertexs.item(
                    endPoint)  # Берем значение вершины до которой был минимальный путь в предыдущей итерации
                a = newVertex - currentVertex2  # Отнимаем от веса соседней верщины вес пути предыдущей
                if a == vertexs.item(s):  # Если разница = весу вершины - значит вес меньше, чем сосдение
                    optimalWay.append(s)  # Записываем вершину
                    endPoint = s  # Теперь пляшем от этой самой вершины

    optimalWay = optimalWay[::-1]
    print(optimalWay)


main()
